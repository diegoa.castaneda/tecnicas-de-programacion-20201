public class Gato extends Animal {

    @Override
    public void hacerRuido() {
        System.out.println("meow meow!");
    }

    @Override
    public void moverse() {
        System.out.println("Avanzo en 4 patas");
    }
}
