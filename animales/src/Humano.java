public class Humano extends Animal implements Volador {
    private String identificacion;
    private String profesion;

    public void trabajar() {
        System.out.println("Trabajo como animal");
    }

    @Override
    public void hacerRuido() {
        System.out.println("blah blah blah!!!");
    }

    @Override
    public void moverse() {
        System.out.println("Camino en mis dos piernas");
    }

    @Override
    public void volar() {
        System.out.println("Vuelo en avión");
    }
}
