public class Principal {

    public static void main(String[] args) {

        Animal gato = new Gato();
        Animal perro = new Perro();
        Animal humano = new Humano();
        Animal paloma = new Paloma();

        gato.hacerRuido();
        perro.hacerRuido();
        humano.hacerRuido();
        paloma.hacerRuido();

        Humano animalConvertido = (Humano) humano;
        animalConvertido.trabajar();
        animalConvertido.volar();

        Volador humanoVolador = (Volador) humano;
        humanoVolador.volar();

        Volador v = (Volador) paloma;
        v.volar();


    }
}
