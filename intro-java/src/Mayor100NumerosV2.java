import javax.swing.*;

public class Mayor100NumerosV2 {

    public static void main(String[] args) {
        //variables
        int numero, cont, mayor;

        //inicio
        mayor = 0;

        for (cont = 0; cont < 10; cont++) {
            numero = Integer.parseInt(JOptionPane.showInputDialog("Ingrese un número:"));
            if (numero > mayor) {
                mayor = numero;
            }
        }
        JOptionPane.showMessageDialog(null, "El mayor es: "+mayor);
    }
}
