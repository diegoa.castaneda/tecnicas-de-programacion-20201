import javax.swing.*;

public class Suma2Numerosv2 {

    public static void main(String[] args) {
        // variables
        int numero1, numero2, suma;

        //inicio
        numero1 = Integer.parseInt(JOptionPane.showInputDialog("Ingrese el primer número:"));
        numero2 = Integer.parseInt(JOptionPane.showInputDialog("Ingrese el segundo número:"));
        suma = numero1 + numero2;

        JOptionPane.showMessageDialog(null, suma);
    }

}
