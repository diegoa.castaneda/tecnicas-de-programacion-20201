import javax.swing.*;

public class Mayor3Numeros {

    public static void main(String[] args) {
        //variables
        int n1, n2, n3;

        //inicio
        n1 = Integer.parseInt(JOptionPane.showInputDialog("Ingrese el primer número:"));
        n2 = Integer.parseInt(JOptionPane.showInputDialog("Ingrese el segundo número:"));
        n3 = Integer.parseInt(JOptionPane.showInputDialog("Ingrese el tercer número:"));

        if (n1 > n2 && n1 > n3) {
            JOptionPane.showMessageDialog(null, "El mayor es: " + n1);
        } else if (n2 > n1 && n2 > n3) {
            JOptionPane.showMessageDialog(null, "El mayor es: " + n2);
        } else if (n3 > n1 && n3 > n2) {
            JOptionPane.showMessageDialog(null, "El mayor es: " + n3);
        } else {
            JOptionPane.showMessageDialog(null, "Son iguales");
        }
    }
}
