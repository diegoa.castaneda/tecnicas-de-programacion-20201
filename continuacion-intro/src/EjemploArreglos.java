import javax.swing.*;

public class EjemploArreglos {

    public static void main(String[] args) {
        //variables
        double[] arreglo;
        double numero;
        int contador, n;

        //inicio
        n = Integer.parseInt(JOptionPane.showInputDialog("Ingrese la cantidad de datos:"));
        arreglo = new double[n];
        for (contador = 0; contador < n; contador++) {
            numero = Double.parseDouble(JOptionPane.showInputDialog("Ingrese un número:"));
            arreglo[contador] = numero;
        }
        String arregloConvertido = obtenerArregloString(arreglo);
        System.out.println(arregloConvertido);
    }

    public static String obtenerArregloString(double[] arreglo) {
        String arregloString = "[ ";
        // forma 2 tradicional
        for (int i = 0; i < arreglo.length; i++) {
            arregloString += (arreglo[i] + ", ");
        }

        arregloString = arregloString
                .substring(0, arregloString.length()-2)
                .concat(" ]");

        return arregloString;
    }

    public static void mostrarArreglo(double[] arreglo){
        // forma 1 funcional
        //Arrays.stream(arreglo).forEach(System.out::println);

        // forma 2 tradicional
        for (int i = 0; i < arreglo.length; i++) {
            System.out.println(arreglo[i]);
        }

        // forma 3: for each
        /*for (double numero : arreglo) {
            System.out.println(numero);
        }*/
    }
}
