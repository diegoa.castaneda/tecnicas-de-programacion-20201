public class Persona {
    String identificacion;
    String nombres;
    int edad;

    // constructor que permite crear personas anónimas
    Persona() {
        this.nombres = "John Doe";
        this.identificacion = "000";
        edad = -1;
    }

    // constructor que recibe los nombres como parámetro
    Persona(String nombres) {
        this.nombres = nombres;
        System.out.println("Im alive!: " + nombres);
    }


    Persona(String identificacion, String nombres) {
        this.identificacion = identificacion;
        this.nombres = nombres;
    }


}
