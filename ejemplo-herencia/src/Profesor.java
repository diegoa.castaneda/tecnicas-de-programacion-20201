public class Profesor extends Persona {
    private String departamento;
    private String titulo;

    public Profesor(){
        super(null, null);
    }

    @Override
    public void caminar() {
        System.out.println("Camino como profesor");
    }


    public String getDepartamento() {
        return departamento;
    }

    public void setDepartamento(String departamento) {
        this.departamento = departamento;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }
}
