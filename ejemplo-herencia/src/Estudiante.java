public class Estudiante extends Persona {
    private double promedio;
    private String programaAcadémico;

    public Estudiante(String id, String nombres) {
        super(id, nombres);
    }

    @Override
    public void caminar() {
        System.out.println("camino como estudiante");
    }

    // método sobreescrito de la clase Persona
    @Override
    public String presentarse() {
        String saludoMadre = super.presentarse();
        return saludoMadre + " y soy estudiante de " + programaAcadémico;
    }

    public double getPromedio() {
        return promedio;
    }

    public void setPromedio(double promedio) {
        this.promedio = promedio;
    }

    public String getProgramaAcadémico() {
        return programaAcadémico;
    }

    public void setProgramaAcadémico(String programaAcadémico) {
        this.programaAcadémico = programaAcadémico;
    }
}
