public class MatrizV2 {

    private static int[][] matriz = {
            {1, 2, 3, 4, 5},
            {2, 3, 4},
            {3, 4, 5, 6}
    };

    public static void main(String[] args) {
        //variables
        int suma, i, j, cont;
        double promedio;

        //inicio
        suma = 0;
        cont = 0;
        for (i = 0; i < matriz.length; i++) {
            for (j = 0; j < matriz[i].length; j++) {
                suma += matriz[i][j];
                cont++;
            }
        }
        promedio = (double) suma / cont;
        System.out.println("El promedio es: "+ promedio);
        imprimirMatriz(matriz);
    }

    public static void imprimirMatriz(int[][] matriz){
        for (int i = 0; i < matriz.length; i++) {
            for (int j = 0; j < matriz[i].length; j++) {
                System.out.print(matriz[i][j]+ " ");
            }
            System.out.println();
        }
    }

}



