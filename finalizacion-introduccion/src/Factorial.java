public class Factorial {

    public static void main(String[] args) {
        System.out.println(Double.MAX_VALUE);
        System.out.println(calcularFactorial(20));
    }

    /**
     *
     * @param n el valor al que se le desea hallar el factorial
     *          debe estar entre 0 y 15
     * @return factorial de n
     */
    public static long calcularFactorial(int n) {
        if (n >= 15) n = 15;
        if (n < 0) n = 0;
        long factorial = 1;
        for (int cont = 2; cont <= n; cont++) {
            factorial *= cont;
        }
        return factorial;
    }
}
