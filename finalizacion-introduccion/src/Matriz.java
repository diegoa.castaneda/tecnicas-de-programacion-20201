import javax.swing.*;

public class Matriz {

    public static void main(String[] args) {
        //variables
        double[][] matriz;
        int m, n, i, j;
        double dato;

        //inicio
        m = Integer
                .parseInt(JOptionPane
                        .showInputDialog("Ingrese la cantidad de filas:"));
        n = Integer
                .parseInt(JOptionPane
                        .showInputDialog("Ingrese la cantidad de columnas:"));

        matriz = new double[m][n];

        for (i = 0; i < matriz.length; i++) {
            for (j = 0; j < matriz[i].length; j++) {
                dato = Double
                        .parseDouble(JOptionPane
                                .showInputDialog("Ingrese un dato : [" + i + ", " + j + "]"));
                matriz[i][j] = dato;
            }
        }

    }
}
